package org.grouplens.lenskit.hello;

import it.unimi.dsi.fastutil.longs.Long2IntMap;
import it.unimi.dsi.fastutil.longs.Long2IntOpenHashMap;
import org.lenskit.data.dao.EventDAO;
import org.lenskit.data.ratings.Rating;
import org.lenskit.inject.Transient;
import org.lenskit.util.io.ObjectStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Provider;

public class PositiveRatingModelBuilder implements Provider<PositiveRatingModel> {
    private static final Logger logger = LoggerFactory.getLogger(PositiveRatingModelBuilder.class);
    private final EventDAO eventDAO;

    @Inject
    public PositiveRatingModelBuilder(@Transient EventDAO evdao) {
        eventDAO = evdao;
    }

    @Override
    public PositiveRatingModel get() {
        logger.info("building positive rating model");
        Long2IntMap counts = new Long2IntOpenHashMap();

        try (ObjectStream<Rating> ratings = eventDAO.streamEvents(Rating.class)) {
            for (Rating r: ratings) {
                long iid = r.getItemId();
                if (r.hasValue() && r.getValue() >= 4.5) {
                    counts.put(iid, counts.get(iid) + 1);
                }
            }
        }

        logger.info("built positive rating model for {} items", counts.size());
        return new PositiveRatingModel(counts);
    }
}
