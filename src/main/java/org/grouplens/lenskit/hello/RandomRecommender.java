package org.grouplens.lenskit.hello;

import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongList;
import it.unimi.dsi.fastutil.longs.LongSet;
import org.lenskit.api.Result;
import org.lenskit.api.ResultList;
import org.lenskit.basic.AbstractItemRecommender;
import org.lenskit.data.dao.ItemDAO;
import org.lenskit.results.Results;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RandomRecommender extends AbstractItemRecommender {
    private final ItemDAO itemDAO;

    @Inject
    public RandomRecommender(ItemDAO items) {
        itemDAO = items;
    }

    @Override
    protected ResultList recommendWithDetails(long user, int n, LongSet candidates, LongSet exclude) {
        LongSet effCands = candidates;
        if (effCands == null) {
            effCands = itemDAO.getItemIds();
        }

        LongList ranked = new LongArrayList(effCands);
        Collections.shuffle(ranked);
        List<Result> results = new ArrayList<Result>(n);
        for (int i = 0; i < n || n <= 0 && i < ranked.size(); i++) {
            results.add(Results.create(ranked.get(i), Double.NaN));
        }
        return Results.newResultList(results);
    }
}
