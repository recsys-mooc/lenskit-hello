package org.grouplens.lenskit.hello;

import it.unimi.dsi.fastutil.longs.Long2IntMap;
import it.unimi.dsi.fastutil.longs.Long2IntOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongSet;
import org.grouplens.grapht.annotation.DefaultProvider;
import org.lenskit.inject.Shareable;

import java.io.Serializable;

@Shareable
@DefaultProvider(PositiveRatingModelBuilder.class)
public class PositiveRatingModel implements Serializable {
    private static final long serialVersionUID = 1L;

    private final Long2IntMap postiveRatingCountMap;

    public PositiveRatingModel(Long2IntMap counts) {
        postiveRatingCountMap = new Long2IntOpenHashMap(counts);
    }

    /**
     * Get the set of all known items.
     * @return The set of all known items.
     */
    public LongSet getItems() {
        return postiveRatingCountMap.keySet();
    }

    /**
     * Get the # of positive ratings for an item.
     * @param item The item ID.
     * @return The # of positive ratings for the item.
     */
    public int getPositiveRatingCount(long item) {
        return postiveRatingCountMap.get(item);
    }
}
