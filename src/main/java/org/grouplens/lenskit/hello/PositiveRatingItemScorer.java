package org.grouplens.lenskit.hello;

import org.lenskit.api.Result;
import org.lenskit.api.ResultMap;
import org.lenskit.basic.AbstractItemScorer;
import org.lenskit.results.Results;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PositiveRatingItemScorer extends AbstractItemScorer {
    private static final Logger logger = LoggerFactory.getLogger(PositiveRatingItemScorer.class);
    private final PositiveRatingModel model;

    @Inject
    public PositiveRatingItemScorer(PositiveRatingModel m) {
        model = m;
    }

    @Nonnull
    @Override
    public ResultMap scoreWithDetails(long user, Collection<Long> items) {
        logger.info("scoring for user {}", user);
        List<Result> results = new ArrayList<>(items.size());

        for (long item: items) {
            logger.debug("scoring item {} for user {}", item, user);
            results.add(Results.create(item, model.getPositiveRatingCount(item)));
        }

        return Results.newResultMap(results);
    }
}
